use v6;

class Reference::Code::Raku {

  method generateBody (|c) {
    # Current call to .set-method is wrong.
    # Generate function from url code. Take URL and find all <var> tags.
    # Take text of HTTP requests, do a subst on all <var> texts
    # to convert them to "%s".
    # A sprintf to convert them to something usable by the generater
    # which will use all NON-optional parameters to populate.

    my $sig-args = q:to/ARGS/.lines.map( ' ' x 23 ~ * ).join("\n");
      |&?ROUTINE.signature
                .params
                .grep( *.named.not )
                .map( ::( *.name ) );
      ARGS

    my $has-params    = c[1]<function>.comb('%s').elems;
    my $function-args =  $has-params ?? ",\n{ $sig-args}" !! ';';

    my $scope-code = c[1]<scopes> ?? qq:to/SCOPES/ !! '';
      return unless self.checkScopes(<
      { c[1]<scopes>.map(' ' x 2 ~ *).join("\n")  }
      >{ c[1]<scopes-any> ?? ', :any' !! '' });
      SCOPES

    my $function-code = qq:to/FUNCTION/;
      { $scope-code
      }my \$function = {
       $has-params ?? 'sprintf ' !! ''
      }'{ c[1]<function> }'{ $function-args }
      FUNCTION

    my $has-opt-params = c[1]<parameters>.grep({ .<optional>.so }).elems;
    my $params-code    = $has-opt-params ?? q:to/PARAMS/ !! '';
      $call.add-param( $p.name.substr(1), ::($p.name) )
        for &?ROUTINE.signature.params.grep( *.named );
      PARAMS

    sprintf q:to/SIGNATURE/, $function-code // '', c.head, $params-code;
      %s
      my $call = $!p.new-call;
      $call.set-method('%s');
      $call.set-function($function);
      %s
      $call.sync;
      from-json( $call.payload );
      SIGNATURE
      #{*}
  }

  # multi sub generateBody('GET'   , $m) {
  # }
  #
  # multi sub generateBody('POST'  , $m) {
  # }
  #
  # multi sub generateBody('PUT'   , $m) {
  # }
  #
  # multi sub generateBody('DELETE', $m) {
  # }
  #
  # multi sub generateBody('PATCH' , $m) {
  # }
  #
  # multi sub generateBody('UPDATE', $m) {
  # }

  method generateClass (
    $class,
    $base-class,
    $base-role,
    $auth-url,
    $url-format,
    $namespace
  ) {
    my @methods;
    for $class.value[] {
      my $methods = $_;
      my $parameter-list = do for $methods<parameters>[] -> $h {
        next unless $h;

        my $desc = ($h<desc> // '').trim;

        (
          do given $h<type> {
            when 'string'     { 'Str()'      }
            when 'integer'    { 'Int()'      }
            when 'boolean'    { 'Bool()'     }
            when 'datetime'   { 'DateTime()' }
            when ''           { ''           }
            when .defined.not { ''           }

            default {
              die "Do not know how to handle the '{ $_ }' type";
            }
          } ~ ' ',
          $h<optional> ?? ':' !! '',
          $h<name>.contains('[]') ?? '@' !! '$' ~ $h<name> ~ ', ',
          $desc ?? " #= { $desc }" !! ''
        ).join('').lines.map( ' ' x 2 ~ * ).join("\n");
      }

      my $body = self.generateBody(
        $methods<method>,
        $methods
      ).lines.map( ' ' x 2 ~ *  ).join("\n");

      # Sort alphabetically
      @methods.push: [
        $methods<name>,
        do {
          my $last-arg = $parameter-list.tail // '';
          $last-arg ~~ s/ ( '$' \w+ ) ','/$0/ if $last-arg;

          my @heads = $parameter-list.head(* - 1) // ();

          qq:to/METHOD/.lines.map( ' ' x 2 ~ * ).join("\n");
             #| { $methods<name> } - { $methods<description> }
            method { $methods<name>  } (
            { ( |@heads, $last-arg ).join("\n ") }
            ) \{
            { $body }
            \}
            METHOD
        }
      ];

      @methods .= sort( *[0] );
      #@methods.map( *[0] ).say;
    }

    my $base = $base-class ?? "is { $base-class }" !! '';
    my $role = $base-role  ?? "does { $base-role }" !! '';
    my @cnn  = $class.key.trim.split(/ \s+ /);
    my $c    = @cnn.map( *.uc ).join('');
    my $file = qq:to/CLASS/;
      use v6;

      use JSON::Fast;
      use Rest::OAuth2::Proxy;

      constant GOOGLE_AUTH_URL = '{ $auth-url }';  # auth-url
      constant { $c }_URL_FORMAT = '{ $url-format }'; # url-format

      class { $namespace }::{ $class.key } { $base } { $role } \{
        has \$!prefix is built;

        submethod BUILD ( :\$!prefix ) \{
        \}

        method class-url-format \{
          { $c }_URL_FORMAT
        \}

        method new (\$client-id, \$prefix) \{
          my \$proxy = Rest::OAuth2::Proxy.new(
            \$client-id,
            GOOGLE_AUTH_URL,
            { $c }_URL_FORMAT
          );

          \$proxy ?? self.bless( :\$proxy, :\$prefix ) !! Nil;
        \}

        {
          # Remember to remove the first 2 spaces, since they
          # have been included, here
          @methods.map( *[1] ).join("\n\n").substr(2)
        }
      \}
      CLASS

    my $new-file = $*inst-dir;
    @cnn = @cnn.map( *.lc.tc );
    say "CNN: { @cnn.gist }";
    @cnn.tail ~= '.pm6';
    $new-file .= add($_) for @cnn;
    qqx«install -d { $new-file.dirname }»;
    my $real-class-name = @cnn.join('::');
    $new-file.spurt($file);
    say "{ $class.key } class definition written to { $new-file.absolute }";
  }

}
