use v6;

unit package Reference::Definitions;

constant PREFIX    is export = 'https://developers.google.com';

our @valid-methods is export = <DELETE GET PATCH POST PUT UPDATE>;

sub loadClass ($class is rw) is export {
  my \oldv = $class;
  $class = try require ::($class);
  $class.^name.say;
  die "Could not load class '{ oldv }'" unless $class !~~ Failure;
  $class
}
