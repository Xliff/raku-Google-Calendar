use v6;

use LWP::Simple;
use DOM::Tiny;

use Reference::Definitions;

class Reference::Page::Google::Mail {

  method spider (%classes) {
    for %classes.pairs.sort( *.key ) -> $class {
      my @methods;
      for $class.value[].sort( *<name> ) -> $methods {
        unless $methods<page> {
          say 'There is no page...';
          next
        }

        my $page-dom = DOM::Tiny.parse(
          LWP::Simple.new.get( "{ PREFIX }{ $methods<page> }" )
        );

        say "{ PREFIX }{ $methods<page> }";

        my $section-rows = $page-dom.find('#body\.PATH_PARAMETERS-table')
                                    .head
                                    .find('tr')
                                    .skip(1)
                                    .cache;

        say "{ $class.key }.{ $methods<name> }: {
               $section-rows.grep( *.tag ).map( *.tag ).gist }";

        my $param-type;
        for $section-rows[] {
          # Request Params
          my $name = .children.head.find('code').head.text;
          my $type = .children.tail.children.head.text( :recurse, :trim );
          my $desc = .children.tail.children.tail.text( :recurse, :trim );
          say "Name: { $name }";
          say "Type: { $type }";
          say "Desc: { $desc }";

          $methods<parameters>.push: (
            name     =>  $name,
            type     =>  $type,
            desc     =>  $desc // '',
            optional => ($desc // '').lc.contains('optional')
          ).Hash;
        }

        my $scopes     = $page-dom.find('#authorization-scopes')
                                  .head;

        my $scope-desc = $scopes.next;
        my $scope-any  = $scope-desc ?? $scope-desc.all-text
                                                   .contains('one' | 'any')
                                                   .so
                                     !! False;

        say "Scope any: { $scope-any }";
        $methods<scope-any> = $scope-any;

        my $scope-rows = $scopes.parent.map( |*.find('li') );

        for $scope-rows[] {
          # Scopes
          if .text( :recurse, :trim ) -> $t {
            if $t.starts-with('https://') {
              $methods<scopes>.push($t);
              say "Found scope for { $methods<name> }: $t ";
            }
          }
        }
      }
    }
  }

}
