use v6;

use Reference::Definitions;

class Reference::Page::Google::Calendar {

  method generate (%classes) {
    for %classes.pairs.sort( *.key ) -> $class {
      my @methods;
      for $class.value[].sort( *<name> ) -> $methods {
        unless $methods<page> {
          say 'There is no page...';
          next
        }

        my $page-dom = DOM::Tiny.parse(
          LWP::Simple.new.get( "{ PREFIX }{ $methods<page> }" )
        );

        my $section-rows = $page-dom.find('#request_parameters')
                                    .head
                                    .find('tr')
                                    .skip(2)
                                    .cache;

        say "{ $class.key }.{ $methods<name> }: {
               $section-rows.map( *.children.map( *.tag ) ).gist }";

        my $param-type;
        for $section-rows.map( *.children ) {
          # Request Params
          if .elems == 3 {
            my $desc = .[2].text( :recurse, :trim );
            $desc .= trim if $desc;
            $methods<parameters>.push: (
              name     => .[0].find('code').head.text,
              type     => .[1].find('code').head.text( :recurse, :trim ),
              desc     =>  $desc // '',
              optional => ($desc // '').lc.contains('optional')
            ).Hash;
          }
        }

        my $scope-rows = $page-dom.find('table.matchpre')
                                  .map( |*.find('tr') );

        for $scope-rows.map( *.children ) {
          # Scopes
          if .elems == 1 {
            if .[0].find('code')[0] -> $c {
              if $c[0].text( :recurse, :trim ) -> $t {
                say "Found scope for { $methods<name> }: $t ";
                $methods<scopes>.push(
                  $t.subst( /\s+/, '', :g ).split('/').tail
                ) if $t.starts-with('https://');
              }
            }
          }
        }
      }
      $class<methods> = @methods;
    }
  }

}
