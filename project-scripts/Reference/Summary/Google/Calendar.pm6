use v6;

use Reference::Definitions;

class Reference::Summary::Google::Calendar {

  method parse ($dom) {
    my (%classes, $param-type);
    for $dom.find('section')[] -> $s {
      #my $header = $s.find('table').map( |*.find('thead').map( |*.find('th') ) );
      my $header = $s.find('table thead th');

      next unless $header.elems == 3;

      for $s.find('h2')[] {
        my $class = .attr('id');

        my @methods;
        for $s.find('table')[] {
          for .find('tr') -> $row {
            my $cells                = $row.find('td');
            my ($method-name, $page) = ( .text, .attr('href') )
              given $cells[0].find('a')[0];

            next unless $method-name;

            say "Method name: { $method-name }";
            say "Ref Page: { $page }";

            my $code = $cells[1].find('code').head;
            my $info = $code.text( :recurse, :trim );

            $info ~~ s/ \s* ( @valid-methods ) \s* //;

            die "Could not determine HTTP method for { $class }.{ $method-name }"
              unless ( my $http-method = $/[0] );

            my @vars = $code.find('var').map( *.text( :recurse, :trim ) );

            (my $func = $info) ~~ s:g/ \s* «@vars» \s* /%s/;
            die "Could not decipher url prefix! for { $class }.{
                 $method-name }" unless $info;

            my $desc = $cells[2].text;
            $desc .= trim if $desc;
            my %data = (
              name        => $method-name,
              page        => $page,
              'method'    => $http-method,
              description => $desc,
              function    => $func
            );

            %classes{ $class }.push: %data if $method-name;

            say "Function: { %data<function> }";
          }
        }
      }
    }

    %classes;
  }

}
