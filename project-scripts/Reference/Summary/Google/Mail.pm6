use v6;

use Reference::Definitions;

class Reference::Summary::Google::Mail {

  method parse ($page-dom, $section = 'v1.users') {
    my (%classes, $param-type);

    for $page-dom.find('section')[] -> $s {
      my $section-id = $s.attr('id');
      next unless $section-id;

      #my $header = $s.find('table').map( |*.find('thead').map( |*.find('th') ) );
      next unless $section-id.starts-with($section);
      my $class-id = $s.attr('id').split('.').map( *.trim );
      my $class =  $class-id.elems < 3
        ?? $class-id.tail
        !! $class-id.subst('METHODS_SUMMARY', '')
                    .split(/ \s+ /)
                    .skip(2)
                    .map( *.lc.tc )
                    .join(' ');

      next unless $class;

      $class .= trim;

      for $s.find('section table tr')[] -> $row {
        my $cells                = $row.find('td');
        my ($method-name, $page) = (
          .text(:recurse, :trim ),
          .attr('href')
        ) given $cells[0].find('a')[0];

        next unless $method-name;

        say "Class: { $class }";
        say "Method name: { $method-name }";
        say "Ref Page: { $page }";

        my $code = $cells[1].find('code').head;
        my $info = $code.text( :recurse, :trim );

        $info ~~ s/ \s* ( @valid-methods ) \s* //;

        die "Could not determine HTTP method for { $class }.{ $method-name }"
          unless ( my $http-method = $/[0] );

        say "I: { $info }";
        $info ~~ m:g/'{' (<-[\}]>+) '}'/;

        my @vars = $/.map( *.Str );
        say "V: { @vars.join(', ') }";

        ( my $func = $info ) ~~ s:g/ @vars /%s/;
        die "Could not decipher url prefix! for { $class }.{
             $method-name }" unless $info;

        my $desc = $cells.tail.children.tail.text( :recurse, :trim );
        my %data = (
          name        => $method-name,
          page        => $page,
          'method'    => $http-method,
          description => $desc,
          function    => $func
        );

        %classes{ $class }.push: %data if $method-name;

        say "Function: { %data<function> }";
      }
    }

    %classes;
  }

}
