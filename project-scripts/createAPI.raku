use v6;

use lib 'project-scripts';

use DOM::Tiny;
use LWP::Simple;

use Reference::Definitions;

sub MAIN (
  :$prefix                 = '',
  :$namespace      is copy = '',
  :$lib-dir                = 'lib',
  :$auth-url               = 'https://www.googleapis.com/auth/',
  :$base-class     is copy = '',
  :$base-role      is copy = '',
  :$url-format     is copy = '',
  :$reference-page is copy = '',
  :$reference      is copy     ,
  :$page           is copy     ,
  :$code           is copy = 'Reference::Code::Raku'
) {

  my @ref-args;
  $url-format = do with $namespace {
    $base-class     = 'Google::Calendar::Base';

    when .ends-with('Mail' | 'GMail') {
      $reference-page =
        'https://developers.google.com/gmail/api/reference/rest'
      unless $reference-page;

      $reference = 'Reference::Summary::Google::Mail';
      $page      = 'Reference::Page::Google::Mail';

      'https://gmail.googleapis.com/gmail/v1/users';
    }

    when .ends-with('Calendar') {
      $reference-page =
        'https://developers.google.com/calendar/api/v3/reference'
      unless $reference-page;

      $reference = 'Reference::Summary::Google::Calendar';
      $page      = 'Reference::Page::Google::Calendar';

      'https://www.googleapis.com/calendar/v3'
    }

    when .ends-with('Sheets') {
      $reference-page =
        'https://developers.google.com/sheets/api/reference/rest'
      unless $reference-page;

      $reference = 'Reference::Summary::Google::Mail';
      $page      = 'Reference::Page::Google::Mail';

      @ref-args = ('v4.spreadsheets');

      'https://sheets.googleapis.com/v4/spreadsheets';
    }

    when .ends-with('Drive') {
      $reference-page =
        'https://developers.google.com/drive/api/v3/reference'
      unless $reference-page;

      $reference = 'Reference::Summary::Google::Calendar';
      $page      = 'Reference::Page::Google::Calendar';

      'https://www.googleapis.com/drive/v3'
    }
  } unless $url-format;

  for $reference, $page, $code -> $_ is rw {
    loadClass($_);
  }

  my $*inst-dir = $lib-dir.IO;
  # cw: Place into Reference::Code::<Lang> as .getNamespaceNodes
  my @namespace-nodes = $namespace.split('::');
  my $ns-head = @namespace-nodes.head;
  $*inst-dir .= add($_) for @namespace-nodes;
  if $*SPEC ~~ IO::Spec::Unix {
    qqx«install -d { $*inst-dir.absolute }»
  }

  say "Retrieving reference from: { $reference-page }";
  my $dom = DOM::Tiny.parse(
    LWP::Simple.new.get($reference-page)
  );

  my %classes = $reference.parse($dom, |@ref-args);

  for %classes -> $class {
    # Move from plural to singular unit generation.
    $page.spider($class);

    $code.generateClass(
      $class,
      $base-class,
      $base-role,
      $auth-url,
      $url-format,
      $namespace
    );
  }
}
