use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = '';  # auth-url
constant CALENDARS_URL_FORMAT = 'https://www.googleapis.com/calendar/v3'; # url-format

class Google::Calendar::Calendars does Google::Roles::Base {
  has $!p;
  has $!prefix is built;

  submethod BUILD ( :$proxy :$!prefix ) {
    $!p = $proxy;
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      CALENDARS_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| clear - Clears a primary calendar. This operation deletes all events associated with the primary calendar of an account.
  method clear (
    Str() $calendarId  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/calendars/%s/clear',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| delete - Deletes a secondary calendar. Use calendars.clear for clearing all events on primary calendars.
  method delete (
    Str() $calendarId  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/calendars/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('DELETE');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - Returns metadata for a calendar.
  method get (
    Str() $calendarId  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
    >);
    my $function = sprintf '/calendars/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| insert - Creates a secondary calendar.
  method insert (
  
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = '/calendars';
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| patch - Updates metadata for a calendar. This method supports patch semantics.
  method patch (
    Str() $calendarId  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/calendars/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PATCH');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| update - Updates metadata for a calendar.
  method update (
    Str() $calendarId  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/calendars/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
