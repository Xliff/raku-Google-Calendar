use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = '';  # auth-url
constant FREEBUSY_URL_FORMAT = 'https://www.googleapis.com/calendar/v3'; # url-format

class Google::Calendar::Freebusy does Google::Roles::Base {
  has $!p;
  has $!prefix is built;

  submethod BUILD ( :$proxy :$!prefix ) {
    $!p = $proxy;
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      FREEBUSY_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| query - Returns free/busy information for a set of calendars.
  method query (
  
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
    >);
    my $function = '/freeBusy';
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
