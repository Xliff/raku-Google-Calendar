use v6.c;

use Google::Roles::Base;

class Google::Calendar::Base {
  also does Google::Roles::Base;

  my @valid-scopes = (
    'calendar' 	                 => [ RW, 'read/write access to Calendars' ],
    'calendar.readonly'	         => [ R,  'read-only access to Calendars'  ],
    'calendar.events'	           => [ RW, 'read/write access to Events'    ],
    'calendar.events.readonly'	 => [ R,  'read-only access to Events'     ],
    'calendar.settings.readonly' => [ R,  'read-only access to Settings'   ]
  );

  method valid-scopes ( :$scopes-only ) {
    my @r = @valid-scopes.pairs;
    @r .= map( *.key ) if $scopes-only;
    @r.List;
  }

}
