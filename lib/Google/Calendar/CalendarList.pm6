use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = '';  # auth-url
constant CALENDARLIST_URL_FORMAT = 'https://www.googleapis.com/calendar/v3'; # url-format

class Google::Calendar::CalendarList does Google::Roles::Base {
  has $!p;
  has $!prefix is built;

  submethod BUILD ( :$proxy :$!prefix ) {
    $!p = $proxy;
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      CALENDARLIST_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| delete - Removes a calendar from the user's calendar list.
  method delete (
    Str() $calendarId  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/users/me/calendarList/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('DELETE');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - Returns a calendar from the user's calendar list.
  method get (
    Str() $calendarId  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
    >);
    my $function = sprintf '/users/me/calendarList/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| insert - Inserts an existing calendar into the user's calendar list.
  method insert (
    Bool() :$colorRgbFormat  #= Whether to use the foregroundColor and backgroundColor fields to write the calendar colors (RGB). If this feature is used, the index-based colorId field will be set to the best matching option automatically. Optional. The default is False.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = '/users/me/calendarList';
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );
    
    $call.sync;
    from-json( $call.payload );
  }

   #| list - Returns the calendars on the user's calendar list.
  method list (
    Int() :$maxResults,  #= Maximum number of entries returned on one result page. By default the value is 100 entries. The page size can never be larger than 250 entries. Optional.
     Str() :$minAccessRole,  #= The minimum access role for the user in the returned entries. Optional. The default is no restriction. Acceptable values are: " freeBusyReader ": The user can read free/busy information. " owner ": The user can read and modify events and access control lists. " reader ": The user can read events that are not private. " writer ": The user can read and modify events.
     Str() :$pageToken,  #= Token specifying which result page to return. Optional.
     Bool() :$showDeleted,  #= Whether to include deleted calendar list entries in the result. Optional. The default is False.
     Bool() :$showHidden,  #= Whether to show hidden entries. Optional. The default is False.
     Str() :$syncToken  #= Token obtained from the nextSyncToken field returned on the last page of results from the previous list request. It makes the result of this list request contain only entries that have changed since then. If only read-only fields such as calendar properties or ACLs have changed, the entry won't be returned. All entries deleted and hidden since the previous list request will always be in the result set and it is not allowed to set showDeleted neither showHidden to False. To ensure client state consistency minAccessRole query parameter cannot be specified together with nextSyncToken. If the syncToken expires, the server will respond with a 410 GONE response code and the client should clear its storage and perform a full synchronization without any syncToken. Learn more about incremental synchronization. Optional. The default is to return all entries.
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
    >);
    my $function = '/users/me/calendarList';
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );
    
    $call.sync;
    from-json( $call.payload );
  }

   #| patch - Updates an existing calendar on the user's calendar list. This method supports patch semantics.
  method patch (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Bool() :$colorRgbFormat  #= Whether to use the foregroundColor and backgroundColor fields to write the calendar colors (RGB). If this feature is used, the index-based colorId field will be set to the best matching option automatically. Optional. The default is False.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/users/me/calendarList/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PATCH');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );
    
    $call.sync;
    from-json( $call.payload );
  }

   #| update - Updates an existing calendar on the user's calendar list.
  method update (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Bool() :$colorRgbFormat  #= Whether to use the foregroundColor and backgroundColor fields to write the calendar colors (RGB). If this feature is used, the index-based colorId field will be set to the best matching option automatically. Optional. The default is False.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/users/me/calendarList/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );
    
    $call.sync;
    from-json( $call.payload );
  }

   #| watch - Watch for changes to CalendarList resources.
  method watch (
  
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
    >);
    my $function = '/users/me/calendarList/watch';
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
