use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant ACL_URL_FORMAT = 'https://www.googleapis.com/calendar/v3'; # url-format

class Google::Calendar::Acl does Google::Roles::Base {
  has $!p;

  submethod BUILD ( :$proxy ) {
    $!p = $proxy;
  }

  method new ($client-id, $prefix, @) {
    my $proxy = self.doAuth($client-id, GOOGLE_AUTH_URL);

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| delete - Deletes an access control rule.
  method delete (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Str() $ruleId  #= ACL rule identifier.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/calendars/%s/acl/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );

    my $call = $!p.new-call;
    $call.set-method('DELETE');
    $call.set-function($function);

    $call.sync;
    from-json( $call.payload );
  }

   #| get - Returns an access control rule.
  method get (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Str() $ruleId  #= ACL rule identifier.
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
    >);
    my $function = sprintf '/calendars/%s/acl/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );

    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);

    $call.sync;
    from-json( $call.payload );
  }

   #| insert - Creates an access control rule.
  method insert (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Bool() :$sendNotifications  #= Whether to send notifications about the calendar sharing change. Optional. The default is True.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/calendars/%s/acl',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );

    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );

    $call.sync;
    from-json( $call.payload );
  }

   #| list - Returns the rules in the access control list for the calendar.
  method list (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Int() :$maxResults,  #= Maximum number of entries returned on one result page. By default the value is 100 entries. The page size can never be larger than 250 entries. Optional.
     Str() :$pageToken,  #= Token specifying which result page to return. Optional.
     Bool() :$showDeleted,  #= Whether to include deleted ACLs in the result. Deleted ACLs are represented by role equal to " none ". Deleted ACLs will always be included if syncToken is provided. Optional. The default is False.
     Str() :$syncToken  #= Token obtained from the nextSyncToken field returned on the last page of results from the previous list request. It makes the result of this list request contain only entries that have changed since then. All entries deleted since the previous list request will always be in the result set and it is not allowed to set showDeleted to False. If the syncToken expires, the server will respond with a 410 GONE response code and the client should clear its storage and perform a full synchronization without any syncToken. Learn more about incremental synchronization. Optional. The default is to return all entries.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/calendars/%s/acl',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );

    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );

    $call.sync;
    from-json( $call.payload );
  }

   #| patch - Updates an access control rule. This method supports patch semantics.
  method patch (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Str() $ruleId,  #= ACL rule identifier.
     Bool() :$sendNotifications  #= Whether to send notifications about the calendar sharing change. Note that there are no notifications on access removal. Optional. The default is True.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/calendars/%s/acl/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );

    my $call = $!p.new-call;
    $call.set-method('PATCH');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );

    $call.sync;
    from-json( $call.payload );
  }

   #| update - Updates an access control rule.
  method update (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Str() $ruleId,  #= ACL rule identifier.
     Bool() :$sendNotifications  #= Whether to send notifications about the calendar sharing change. Note that there are no notifications on access removal. Optional. The default is True.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/calendars/%s/acl/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );

    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );

    $call.sync;
    from-json( $call.payload );
  }

   #| watch - Watch for changes to ACL resources.
  method watch (
    Str() $calendarId  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
  ) {
    return unless self.checkScopes(<
      calendar
    >);
    my $function = sprintf '/calendars/%s/acl/watch',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );

    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);

    $call.sync;
    from-json( $call.payload );
  }
}
