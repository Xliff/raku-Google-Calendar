use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = '';  # auth-url
constant COLORS_URL_FORMAT = 'https://www.googleapis.com/calendar/v3'; # url-format

class Google::Calendar::Colors does Google::Roles::Base {
  has $!p;
  has $!prefix is built;

  submethod BUILD ( :$proxy :$!prefix ) {
    $!p = $proxy;
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      COLORS_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| get - Returns the color definitions for calendars and events.
  method get (
  
  ) {
    return unless self.checkScopes(<
      calendar
      calendar.readonly
    >);
    my $function = '/colors';
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
