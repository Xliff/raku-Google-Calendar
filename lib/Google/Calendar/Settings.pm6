use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = '';  # auth-url
constant SETTINGS_URL_FORMAT = 'https://www.googleapis.com/calendar/v3'; # url-format

class Google::Calendar::Settings does Google::Roles::Base {
  has $!p;
  has $!prefix is built;

  submethod BUILD ( :$proxy :$!prefix ) {
    $!p = $proxy;
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      SETTINGS_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| get - Returns a single user setting.
  method get (
    Str() $setting  #= The id of the user setting.
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
      calendar.settings.readonly
    >);
    my $function = sprintf '/users/me/settings/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| list - Returns all user settings for the authenticated user.
  method list (
    Int() :$maxResults,  #= Maximum number of entries returned on one result page. By default the value is 100 entries. The page size can never be larger than 250 entries. Optional.
     Str() :$pageToken,  #= Token specifying which result page to return. Optional.
     Str() :$syncToken  #= Token obtained from the nextSyncToken field returned on the last page of results from the previous list request. It makes the result of this list request contain only entries that have changed since then. If the syncToken expires, the server will respond with a 410 GONE response code and the client should clear its storage and perform a full synchronization without any syncToken. Learn more about incremental synchronization. Optional. The default is to return all entries.
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
      calendar.settings.readonly
    >);
    my $function = '/users/me/settings';
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );
    
    $call.sync;
    from-json( $call.payload );
  }

   #| watch - Watch for changes to Settings resources.
  method watch (
  
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
      calendar.settings.readonly
    >);
    my $function = '/users/me/settings/watch';
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
