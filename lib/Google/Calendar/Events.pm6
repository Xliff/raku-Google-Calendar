use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = '';  # auth-url
constant EVENTS_URL_FORMAT = 'https://www.googleapis.com/calendar/v3'; # url-format

class Google::Calendar::Events does Google::Roles::Base {
  has $!p;
  has $!prefix is built;

  submethod BUILD ( :$proxy :$!prefix ) {
    $!p = $proxy;
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      EVENTS_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| delete - Deletes an event.
  method delete (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Str() $eventId,  #= Event identifier.
     Bool() $sendNotifications,  #= Deprecated. Please use sendUpdates instead. Whether to send notifications about the deletion of the event. Note that some emails might still be sent even if you set the value to false. The default is false.
     Str() $sendUpdates  #= Guests who should receive notifications about the deletion of the event. Acceptable values are: " all ": Notifications are sent to all guests. " externalOnly ": Notifications are sent to non-Google Calendar guests only. " none ": No notifications are sent. For calendar migration tasks, consider using the Events.import method instead.
  ) {
    return unless self.checkScopes(<
      calendar
      calendar.events
    >);
    my $function = sprintf '/calendars/%s/events/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('DELETE');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - Returns an event.
  method get (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Str() $eventId,  #= Event identifier.
     Bool() $alwaysIncludeEmail,  #= Deprecated and ignored. A value will always be returned in the email field for the organizer, creator and attendees, even if no real email address is available (i.e. a generated, non-working value will be provided).
     Int() :$maxAttendees,  #= The maximum number of attendees to include in the response. If there are more than the specified number of attendees, only the participant is returned. Optional.
     Str() :$timeZone  #= Time zone used in the response. Optional. The default is the time zone of the calendar.
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
      calendar.events.readonly
      calendar.events
    >);
    my $function = sprintf '/calendars/%s/events/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );
    
    $call.sync;
    from-json( $call.payload );
  }

   #| import - Imports an event. This operation is used to add a private copy of an existing event to a calendar.
  method import (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Int() $conferenceDataVersion,  #= Version number of conference data supported by the API client. Version 0 assumes no conference data support and ignores conference data in the event's body. Version 1 enables support for copying of ConferenceData as well as for creating new conferences using the createRequest field of conferenceData. The default is 0. Acceptable values are 0 to 1, inclusive.
     Bool() :$supportsAttachments  #= Whether API client performing operation supports event attachments. Optional. The default is False.
  ) {
    return unless self.checkScopes(<
      calendar
      calendar.events
    >);
    my $function = sprintf '/calendars/%s/events/import',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );
    
    $call.sync;
    from-json( $call.payload );
  }

   #| insert - Creates an event.
  method insert (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Int() $conferenceDataVersion,  #= Version number of conference data supported by the API client. Version 0 assumes no conference data support and ignores conference data in the event's body. Version 1 enables support for copying of ConferenceData as well as for creating new conferences using the createRequest field of conferenceData. The default is 0. Acceptable values are 0 to 1, inclusive.
     Int() :$maxAttendees,  #= The maximum number of attendees to include in the response. If there are more than the specified number of attendees, only the participant is returned. Optional.
     Bool() $sendNotifications,  #= Deprecated. Please use sendUpdates instead. Whether to send notifications about the creation of the new event. Note that some emails might still be sent even if you set the value to false. The default is false.
     Str() $sendUpdates,  #= Whether to send notifications about the creation of the new event. Note that some emails might still be sent. The default is false. Acceptable values are: " all ": Notifications are sent to all guests. " externalOnly ": Notifications are sent to non-Google Calendar guests only. " none ": No notifications are sent. Warning: Using the value none can have significant adverse effects, including events not syncing to external calendars or events being lost altogether for some users. For calendar migration tasks, consider using the events.import method instead.
     Bool() :$supportsAttachments  #= Whether API client performing operation supports event attachments. Optional. The default is False.
  ) {
    return unless self.checkScopes(<
      calendar
      calendar.events
    >);
    my $function = sprintf '/calendars/%s/events',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );
    
    $call.sync;
    from-json( $call.payload );
  }

   #| instances - Returns instances of the specified recurring event.
  method instances (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Str() $eventId,  #= Recurring event identifier.
     Bool() $alwaysIncludeEmail,  #= Deprecated and ignored. A value will always be returned in the email field for the organizer, creator and attendees, even if no real email address is available (i.e. a generated, non-working value will be provided).
     Int() :$maxAttendees,  #= The maximum number of attendees to include in the response. If there are more than the specified number of attendees, only the participant is returned. Optional.
     Int() :$maxResults,  #= Maximum number of events returned on one result page. By default the value is 250 events. The page size can never be larger than 2500 events. Optional.
     Str() :$originalStart,  #= The original start time of the instance in the result. Optional.
     Str() :$pageToken,  #= Token specifying which result page to return. Optional.
     Bool() :$showDeleted,  #= Whether to include deleted events (with status equals " cancelled ") in the result. Cancelled instances of recurring events will still be included if singleEvents is False. Optional. The default is False.
     DateTime() :$timeMax,  #= Upper bound (exclusive) for an event's start time to filter by. Optional. The default is not to filter by start time. Must be an RFC3339 timestamp with mandatory time zone offset.
     DateTime() :$timeMin,  #= Lower bound (inclusive) for an event's end time to filter by. Optional. The default is not to filter by end time. Must be an RFC3339 timestamp with mandatory time zone offset.
     Str() :$timeZone  #= Time zone used in the response. Optional. The default is the time zone of the calendar.
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
      calendar.events.readonly
      calendar.events
    >);
    my $function = sprintf '/calendars/%s/events/%s/instances',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );
    
    $call.sync;
    from-json( $call.payload );
  }

   #| list - Returns events on the specified calendar.
  method list (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Bool() $alwaysIncludeEmail,  #= Deprecated and ignored. A value will always be returned in the email field for the organizer, creator and attendees, even if no real email address is available (i.e. a generated, non-working value will be provided).
     Str() :$iCalUID,  #= Specifies event ID in the iCalendar format to be included in the response. Optional.
     Int() :$maxAttendees,  #= The maximum number of attendees to include in the response. If there are more than the specified number of attendees, only the participant is returned. Optional.
     Int() :$maxResults,  #= Maximum number of events returned on one result page. The number of events in the resulting page may be less than this value, or none at all, even if there are more events matching the query. Incomplete pages can be detected by a non-empty nextPageToken field in the response. By default the value is 250 events. The page size can never be larger than 2500 events. Optional.
     Str() :$orderBy,  #= The order of the events returned in the result. Optional. The default is an unspecified, stable order. Acceptable values are: " startTime ": Order by the start date/time (ascending). This is only available when querying single events (i.e. the parameter singleEvents is True) " updated ": Order by last modification time (ascending).
     Str() :$pageToken,  #= Token specifying which result page to return. Optional.
     Str() $privateExtendedProperty,  #= Extended properties constraint specified as propertyName=value. Matches only private properties. This parameter might be repeated multiple times to return events that match all given constraints.
     Str() :$q,  #= Free text search terms to find events that match these terms in the following fields: summary, description, location, attendee's displayName, attendee's email. Optional.
     Str() $sharedExtendedProperty,  #= Extended properties constraint specified as propertyName=value. Matches only shared properties. This parameter might be repeated multiple times to return events that match all given constraints.
     Bool() :$showDeleted,  #= Whether to include deleted events (with status equals " cancelled ") in the result. Cancelled instances of recurring events (but not the underlying recurring event) will still be included if showDeleted and singleEvents are both False. If showDeleted and singleEvents are both True, only single instances of deleted events (but not the underlying recurring events) are returned. Optional. The default is False.
     Bool() :$showHiddenInvitations,  #= Whether to include hidden invitations in the result. Optional. The default is False.
     Bool() :$singleEvents,  #= Whether to expand recurring events into instances and only return single one-off events and instances of recurring events, but not the underlying recurring events themselves. Optional. The default is False.
     Str() :$syncToken,  #= Token obtained from the nextSyncToken field returned on the last page of results from the previous list request. It makes the result of this list request contain only entries that have changed since then. All events deleted since the previous list request will always be in the result set and it is not allowed to set showDeleted to False. There are several query parameters that cannot be specified together with nextSyncToken to ensure consistency of the client state. These are: iCalUID orderBy privateExtendedProperty q sharedExtendedProperty timeMin timeMax updatedMin If the syncToken expires, the server will respond with a 410 GONE response code and the client should clear its storage and perform a full synchronization without any syncToken. Learn more about incremental synchronization. Optional. The default is to return all entries.
     DateTime() :$timeMax,  #= Upper bound (exclusive) for an event's start time to filter by. Optional. The default is not to filter by start time. Must be an RFC3339 timestamp with mandatory time zone offset, for example, 2011-06-03T10:00:00-07:00, 2011-06-03T10:00:00Z. Milliseconds may be provided but are ignored. If timeMin is set, timeMax must be greater than timeMin.
     DateTime() :$timeMin,  #= Lower bound (exclusive) for an event's end time to filter by. Optional. The default is not to filter by end time. Must be an RFC3339 timestamp with mandatory time zone offset, for example, 2011-06-03T10:00:00-07:00, 2011-06-03T10:00:00Z. Milliseconds may be provided but are ignored. If timeMax is set, timeMin must be smaller than timeMax.
     Str() :$timeZone,  #= Time zone used in the response. Optional. The default is the time zone of the calendar.
     DateTime() :$updatedMin  #= Lower bound for an event's last modification time (as a RFC3339 timestamp) to filter by. When specified, entries deleted since this time will always be included regardless of showDeleted. Optional. The default is not to filter by last modification time.
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
      calendar.events.readonly
      calendar.events
    >);
    my $function = sprintf '/calendars/%s/events',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );
    
    $call.sync;
    from-json( $call.payload );
  }

   #| move - Moves an event to another calendar, i.e. changes an event's organizer.
  method move (
    Str() $calendarId,  #= Calendar identifier of the source calendar where the event currently is on.
     Str() $eventId,  #= Event identifier.
     Str() $destination,  #= Calendar identifier of the target calendar where the event is to be moved to.
     Bool() $sendNotifications,  #= Deprecated. Please use sendUpdates instead. Whether to send notifications about the change of the event's organizer. Note that some emails might still be sent even if you set the value to false. The default is false.
     Str() $sendUpdates  #= Guests who should receive notifications about the change of the event's organizer. Acceptable values are: " all ": Notifications are sent to all guests. " externalOnly ": Notifications are sent to non-Google Calendar guests only. " none ": No notifications are sent. For calendar migration tasks, consider using the Events.import method instead.
  ) {
    return unless self.checkScopes(<
      calendar
      calendar.events
    >);
    my $function = sprintf '/calendars/%s/events/%s/move',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| patch - Updates an event. This method supports patch semantics. The field values you specify replace the existing values. Fields that you don’t specify in the request remain unchanged. Array fields, if specified, overwrite the existing arrays; this discards any previous array elements.
  method patch (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Str() $eventId,  #= Event identifier.
     Bool() $alwaysIncludeEmail,  #= Deprecated and ignored. A value will always be returned in the email field for the organizer, creator and attendees, even if no real email address is available (i.e. a generated, non-working value will be provided).
     Int() $conferenceDataVersion,  #= Version number of conference data supported by the API client. Version 0 assumes no conference data support and ignores conference data in the event's body. Version 1 enables support for copying of ConferenceData as well as for creating new conferences using the createRequest field of conferenceData. The default is 0. Acceptable values are 0 to 1, inclusive.
     Int() :$maxAttendees,  #= The maximum number of attendees to include in the response. If there are more than the specified number of attendees, only the participant is returned. Optional.
     Bool() $sendNotifications,  #= Deprecated. Please use sendUpdates instead. Whether to send notifications about the event update (for example, description changes, etc.). Note that some emails might still be sent even if you set the value to false. The default is false.
     Str() $sendUpdates,  #= Guests who should receive notifications about the event update (for example, title changes, etc.). Acceptable values are: " all ": Notifications are sent to all guests. " externalOnly ": Notifications are sent to non-Google Calendar guests only. " none ": No notifications are sent. For calendar migration tasks, consider using the Events.import method instead.
     Bool() :$supportsAttachments  #= Whether API client performing operation supports event attachments. Optional. The default is False.
  ) {
    return unless self.checkScopes(<
      calendar
      calendar.events
    >);
    my $function = sprintf '/calendars/%s/events/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PATCH');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );
    
    $call.sync;
    from-json( $call.payload );
  }

   #| quickAdd - Creates an event based on a simple text string.
  method quickAdd (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Str() $text,  #= The text describing the event to be created.
     Bool() $sendNotifications,  #= Deprecated. Please use sendUpdates instead. Whether to send notifications about the creation of the event. Note that some emails might still be sent even if you set the value to false. The default is false.
     Str() $sendUpdates  #= Guests who should receive notifications about the creation of the new event. Acceptable values are: " all ": Notifications are sent to all guests. " externalOnly ": Notifications are sent to non-Google Calendar guests only. " none ": No notifications are sent. For calendar migration tasks, consider using the Events.import method instead.
  ) {
    return unless self.checkScopes(<
      calendar
      calendar.events
    >);
    my $function = sprintf '/calendars/%s/events/quickAdd',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| update - Updates an event.
  method update (
    Str() $calendarId,  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
     Str() $eventId,  #= Event identifier.
     Bool() $alwaysIncludeEmail,  #= Deprecated and ignored. A value will always be returned in the email field for the organizer, creator and attendees, even if no real email address is available (i.e. a generated, non-working value will be provided).
     Int() $conferenceDataVersion,  #= Version number of conference data supported by the API client. Version 0 assumes no conference data support and ignores conference data in the event's body. Version 1 enables support for copying of ConferenceData as well as for creating new conferences using the createRequest field of conferenceData. The default is 0. Acceptable values are 0 to 1, inclusive.
     Int() :$maxAttendees,  #= The maximum number of attendees to include in the response. If there are more than the specified number of attendees, only the participant is returned. Optional.
     Bool() $sendNotifications,  #= Deprecated. Please use sendUpdates instead. Whether to send notifications about the event update (for example, description changes, etc.). Note that some emails might still be sent even if you set the value to false. The default is false.
     Str() $sendUpdates,  #= Guests who should receive notifications about the event update (for example, title changes, etc.). Acceptable values are: " all ": Notifications are sent to all guests. " externalOnly ": Notifications are sent to non-Google Calendar guests only. " none ": No notifications are sent. For calendar migration tasks, consider using the Events.import method instead.
     Bool() :$supportsAttachments  #= Whether API client performing operation supports event attachments. Optional. The default is False.
  ) {
    return unless self.checkScopes(<
      calendar
      calendar.events
    >);
    my $function = sprintf '/calendars/%s/events/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    $call.add-param( $p.name.substr(1), ::($p.name) )
      for &?ROUTINE.signature.params.grep( *.named );
    
    $call.sync;
    from-json( $call.payload );
  }

   #| watch - Watch for changes to Events resources.
  method watch (
    Str() $calendarId  #= Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the " primary " keyword.
  ) {
    return unless self.checkScopes(<
      calendar.readonly
      calendar
      calendar.events.readonly
      calendar.events
    >);
    my $function = sprintf '/calendars/%s/events/watch',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
