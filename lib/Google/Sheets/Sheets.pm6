use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = 'https://www.googleapis.com/auth/';  # auth-url
constant SHEETS_URL_FORMAT = 'https://sheets.googleapis.com/v4/spreadsheets'; # url-format

class Google::Sheets::Sheets is Google::Calendar::Base  {
  has $!prefix is built;

  submethod BUILD ( :$!prefix ) {
  }

  method class-url-format {
    SHEETS_URL_FORMAT
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      SHEETS_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| copyTo - 
  method copyTo (
    Str() $spreadsheetId,  #= The ID of the spreadsheet containing the sheet to copy.
     Int() $sheetId  #= The ID of the sheet to copy.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/sheets/%s:copyTo',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| copyTo - 
  method copyTo (
    Str() $spreadsheetId,  #= The ID of the spreadsheet containing the sheet to copy.
     Int() $sheetId  #= The ID of the sheet to copy.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/sheets/%s:copyTo',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
