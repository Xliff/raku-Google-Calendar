use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = 'https://www.googleapis.com/auth/';  # auth-url
constant SPREADSHEETS_URL_FORMAT = 'https://sheets.googleapis.com/v4/spreadsheets'; # url-format

class Google::Sheets::spreadsheets is Google::Calendar::Base  {
  has $!prefix is built;

  submethod BUILD ( :$!prefix ) {
  }

  method class-url-format {
    SPREADSHEETS_URL_FORMAT
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      SPREADSHEETS_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| batchUpdate - 
  method batchUpdate (
    Str() $spreadsheetId  #= The spreadsheet to apply the updates to.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s:batchUpdate',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| create - 
  method create (
  
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = '/v4/spreadsheets';
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - 
  method get (
    Str() $spreadsheetId  #= The spreadsheet to request.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.readonly
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
      https://www.googleapis.com/auth/spreadsheets.readonly
    >);
    my $function = sprintf '/v4/spreadsheets/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| getByDataFilter - 
  method getByDataFilter (
    Str() $spreadsheetId  #= The spreadsheet to request.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s:getByDataFilter',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
