use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = 'https://www.googleapis.com/auth/';  # auth-url
constant VALUES_URL_FORMAT = 'https://sheets.googleapis.com/v4/spreadsheets'; # url-format

class Google::Sheets::Values is Google::Calendar::Base  {
  has $!prefix is built;

  submethod BUILD ( :$!prefix ) {
  }

  method class-url-format {
    VALUES_URL_FORMAT
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      VALUES_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| append - 
  method append (
    Str() $spreadsheetId,  #= The ID of the spreadsheet to update.
     Str() $range  #= The A1 notation of a range to search for a logical table of data. Values are appended after the last row of the table.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values/%s:append',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| append - 
  method append (
    Str() $spreadsheetId,  #= The ID of the spreadsheet to update.
     Str() $range  #= The A1 notation of a range to search for a logical table of data. Values are appended after the last row of the table.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values/%s:append',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| batchClear - 
  method batchClear (
    Str() $spreadsheetId  #= The ID of the spreadsheet to update.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values:batchClear',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| batchClear - 
  method batchClear (
    Str() $spreadsheetId  #= The ID of the spreadsheet to update.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values:batchClear',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| batchClearByDataFilter - 
  method batchClearByDataFilter (
    Str() $spreadsheetId  #= The ID of the spreadsheet to update.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values:batchClearByDataFilter',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| batchClearByDataFilter - 
  method batchClearByDataFilter (
    Str() $spreadsheetId  #= The ID of the spreadsheet to update.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values:batchClearByDataFilter',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| batchGet - 
  method batchGet (
    Str() $spreadsheetId  #= The ID of the spreadsheet to retrieve data from.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.readonly
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
      https://www.googleapis.com/auth/spreadsheets.readonly
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values:batchGet',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| batchGet - 
  method batchGet (
    Str() $spreadsheetId  #= The ID of the spreadsheet to retrieve data from.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.readonly
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
      https://www.googleapis.com/auth/spreadsheets.readonly
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values:batchGet',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| batchGetByDataFilter - 
  method batchGetByDataFilter (
    Str() $spreadsheetId  #= The ID of the spreadsheet to retrieve data from.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values:batchGetByDataFilter',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| batchGetByDataFilter - 
  method batchGetByDataFilter (
    Str() $spreadsheetId  #= The ID of the spreadsheet to retrieve data from.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values:batchGetByDataFilter',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| batchUpdate - 
  method batchUpdate (
    Str() $spreadsheetId  #= The ID of the spreadsheet to update.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values:batchUpdate',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| batchUpdate - 
  method batchUpdate (
    Str() $spreadsheetId  #= The ID of the spreadsheet to update.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values:batchUpdate',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| batchUpdateByDataFilter - 
  method batchUpdateByDataFilter (
    Str() $spreadsheetId  #= The ID of the spreadsheet to update.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values:batchUpdateByDataFilter',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| batchUpdateByDataFilter - 
  method batchUpdateByDataFilter (
    Str() $spreadsheetId  #= The ID of the spreadsheet to update.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values:batchUpdateByDataFilter',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| clear - 
  method clear (
    Str() $spreadsheetId,  #= The ID of the spreadsheet to update.
     Str() $range  #= The A1 notation or R1C1 notation of the values to clear.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values/%s:clear',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| clear - 
  method clear (
    Str() $spreadsheetId,  #= The ID of the spreadsheet to update.
     Str() $range  #= The A1 notation or R1C1 notation of the values to clear.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values/%s:clear',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - 
  method get (
    Str() $spreadsheetId,  #= The ID of the spreadsheet to retrieve data from.
     Str() $range  #= The A1 notation or R1C1 notation of the range to retrieve values from.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.readonly
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
      https://www.googleapis.com/auth/spreadsheets.readonly
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - 
  method get (
    Str() $spreadsheetId,  #= The ID of the spreadsheet to retrieve data from.
     Str() $range  #= The A1 notation or R1C1 notation of the range to retrieve values from.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.readonly
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
      https://www.googleapis.com/auth/spreadsheets.readonly
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| update - 
  method update (
    Str() $spreadsheetId,  #= The ID of the spreadsheet to update.
     Str() $range  #= The A1 notation of the values to update.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| update - 
  method update (
    Str() $spreadsheetId,  #= The ID of the spreadsheet to update.
     Str() $range  #= The A1 notation of the values to update.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/values/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
