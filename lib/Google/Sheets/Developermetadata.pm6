use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = 'https://www.googleapis.com/auth/';  # auth-url
constant DEVELOPERMETADATA_URL_FORMAT = 'https://sheets.googleapis.com/v4/spreadsheets'; # url-format

class Google::Sheets::Developermetadata is Google::Calendar::Base  {
  has $!prefix is built;

  submethod BUILD ( :$!prefix ) {
  }

  method class-url-format {
    DEVELOPERMETADATA_URL_FORMAT
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      DEVELOPERMETADATA_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| get - 
  method get (
    Str() $spreadsheetId,  #= The ID of the spreadsheet to retrieve metadata from.
     Int() $metadataId  #= The ID of the developer metadata to retrieve.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/developerMetadata/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - 
  method get (
    Str() $spreadsheetId,  #= The ID of the spreadsheet to retrieve metadata from.
     Int() $metadataId  #= The ID of the developer metadata to retrieve.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/developerMetadata/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| search - DataFilter
  method search (
    Str() $spreadsheetId  #= The ID of the spreadsheet to retrieve metadata from.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/developerMetadata:search',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| search - DataFilter
  method search (
    Str() $spreadsheetId  #= The ID of the spreadsheet to retrieve metadata from.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/drive
      https://www.googleapis.com/auth/drive.file
      https://www.googleapis.com/auth/spreadsheets
    >);
    my $function = sprintf '/v4/spreadsheets/%s/developerMetadata:search',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
