use v6.c;

our enum ACCESS is export <R RW>;

constant GOOGLE_AUTH_URL = 'https://www.googleapis.com/auth/';  # auth-url

my $web-view;

role Google::Roles::Base {
  has @!scopes;
  has $!proxy;

  method doAuth ($redirect-uri, $client-id, $code-challenge, $scopes) {
    my $auth-url = self!prepAuthUrl(
      $redirect-uri,
      $client-id,
      $code-challenge,
      $scopes
    );

    $!proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      ACL_URL_FORMAT
    );

    # If Webkit enabled:
    # - Open webview
    # - Allow user to login
    # - Grab access token on redirect
    # Else:
    # - Prompt user to enter access token or 'Q to exit.
    #
    # - Set access token on proxy
  }

  method !prepAuthUrl(
    $redirect-uri,
    $client-secret,
    $code-challenge,
    $scopes
  ) {
    $!proxy.build-login-url-full(
      $redirect-uri,
      GLib::HashTable.new({
        client-id      => $proxy.client-id,
        client-secret  => $client-secret,
        code-challenge => $code-challenge,
        scopes         => @!scopes = $scopes,
      })
    );
  }

  method checkScopes ($to-match, :$any = False) {
    $any ?? @!scopes.any eq $to-match.any;
         !! @!scopes.all eq $to-match.any;
  }

}

INIT {
  $web-view = try require ::('Webkit::WebView');
  warn 'Webkit::WebView not found. Proceeding with manual operation...'
    unless $web-view !~~ Failure;
}
