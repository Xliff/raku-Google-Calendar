use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = 'https://www.googleapis.com/auth/';  # auth-url
constant SETTINGS_URL_FORMAT = 'https://gmail.googleapis.com/gmail/v1/users'; # url-format

class Google::Mail::Settings is Google::Calendar::Base  {
  has $!prefix is built;

  submethod BUILD ( :$!prefix ) {
  }

  method class-url-format {
    SETTINGS_URL_FORMAT
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      SETTINGS_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| getAutoForwarding - 
  method getAutoForwarding (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/autoForwarding',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| getAutoForwarding - 
  method getAutoForwarding (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/autoForwarding',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| getImap - 
  method getImap (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/imap',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| getImap - 
  method getImap (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/imap',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| getLanguage - 
  method getLanguage (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/language',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| getLanguage - 
  method getLanguage (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/language',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| getPop - 
  method getPop (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/pop',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| getPop - 
  method getPop (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/pop',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| getVacation - 
  method getVacation (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/vacation',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| getVacation - 
  method getVacation (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/vacation',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| updateAutoForwarding - 
  method updateAutoForwarding (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/autoForwarding',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| updateAutoForwarding - 
  method updateAutoForwarding (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/autoForwarding',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| updateImap - 
  method updateImap (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/imap',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| updateImap - 
  method updateImap (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/imap',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| updateLanguage - 
  method updateLanguage (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/language',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| updateLanguage - 
  method updateLanguage (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/language',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| updatePop - 
  method updatePop (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/pop',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| updatePop - 
  method updatePop (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/pop',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| updateVacation - 
  method updateVacation (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/vacation',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| updateVacation - 
  method updateVacation (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/vacation',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
