use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = 'https://www.googleapis.com/auth/';  # auth-url
constant SETTINGSSENDASSMIMEINFO_URL_FORMAT = 'https://gmail.googleapis.com/gmail/v1/users'; # url-format

class Google::Mail::Settings Sendas Smimeinfo is Google::Calendar::Base  {
  has $!prefix is built;

  submethod BUILD ( :$!prefix ) {
  }

  method class-url-format {
    SETTINGSSENDASSMIMEINFO_URL_FORMAT
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      SETTINGSSENDASSMIMEINFO_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| delete - 
  method delete (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $sendAsEmail,  #= The email address that appears in the "From:" header for mail sent using this alias.
     Str() $id  #= The immutable ID for the SmimeInfo.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/sendAs/%s/smimeInfo/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('DELETE');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| delete - 
  method delete (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $sendAsEmail,  #= The email address that appears in the "From:" header for mail sent using this alias.
     Str() $id  #= The immutable ID for the SmimeInfo.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/sendAs/%s/smimeInfo/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('DELETE');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - 
  method get (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $sendAsEmail,  #= The email address that appears in the "From:" header for mail sent using this alias.
     Str() $id  #= The immutable ID for the SmimeInfo.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/sendAs/%s/smimeInfo/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - 
  method get (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $sendAsEmail,  #= The email address that appears in the "From:" header for mail sent using this alias.
     Str() $id  #= The immutable ID for the SmimeInfo.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/sendAs/%s/smimeInfo/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| insert - 
  method insert (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $sendAsEmail  #= The email address that appears in the "From:" header for mail sent using this alias.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/sendAs/%s/smimeInfo',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| insert - 
  method insert (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $sendAsEmail  #= The email address that appears in the "From:" header for mail sent using this alias.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/sendAs/%s/smimeInfo',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| list - 
  method list (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $sendAsEmail  #= The email address that appears in the "From:" header for mail sent using this alias.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/sendAs/%s/smimeInfo',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| list - 
  method list (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $sendAsEmail  #= The email address that appears in the "From:" header for mail sent using this alias.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/sendAs/%s/smimeInfo',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| setDefault - 
  method setDefault (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $sendAsEmail,  #= The email address that appears in the "From:" header for mail sent using this alias.
     Str() $id  #= The immutable ID for the SmimeInfo.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/sendAs/%s/smimeInfo/%s/setDefault',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| setDefault - 
  method setDefault (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $sendAsEmail,  #= The email address that appears in the "From:" header for mail sent using this alias.
     Str() $id  #= The immutable ID for the SmimeInfo.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/sendAs/%s/smimeInfo/%s/setDefault',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
