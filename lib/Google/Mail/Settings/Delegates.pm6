use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = 'https://www.googleapis.com/auth/';  # auth-url
constant SETTINGSDELEGATES_URL_FORMAT = 'https://gmail.googleapis.com/gmail/v1/users'; # url-format

class Google::Mail::Settings Delegates is Google::Calendar::Base  {
  has $!prefix is built;

  submethod BUILD ( :$!prefix ) {
  }

  method class-url-format {
    SETTINGSDELEGATES_URL_FORMAT
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      SETTINGSDELEGATES_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| create - accepted
  method create (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/delegates',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| create - accepted
  method create (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/delegates',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| delete - 
  method delete (
    Str() $userId,  #= User's email address. The special value "me" can be used to indicate the authenticated user.
     Str() $delegateEmail  #= The email address of the user to be removed as a delegate.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/delegates/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('DELETE');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| delete - 
  method delete (
    Str() $userId,  #= User's email address. The special value "me" can be used to indicate the authenticated user.
     Str() $delegateEmail  #= The email address of the user to be removed as a delegate.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.sharing
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/delegates/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('DELETE');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - 
  method get (
    Str() $userId,  #= User's email address. The special value "me" can be used to indicate the authenticated user.
     Str() $delegateEmail  #= The email address of the user whose delegate relationship is to be retrieved.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/delegates/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - 
  method get (
    Str() $userId,  #= User's email address. The special value "me" can be used to indicate the authenticated user.
     Str() $delegateEmail  #= The email address of the user whose delegate relationship is to be retrieved.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/delegates/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| list - 
  method list (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/delegates',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| list - 
  method list (
    Str() $userId  #= User's email address. The special value "me" can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://www.googleapis.com/auth/gmail.settings.basic
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/settings/delegates',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
