use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = 'https://www.googleapis.com/auth/';  # auth-url
constant THREADS_URL_FORMAT = 'https://gmail.googleapis.com/gmail/v1/users'; # url-format

class Google::Mail::Threads is Google::Calendar::Base  {
  has $!prefix is built;

  submethod BUILD ( :$!prefix ) {
  }

  method class-url-format {
    THREADS_URL_FORMAT
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      THREADS_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| delete - 
  method delete (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= ID of the Thread to delete.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
    >);
    my $function = sprintf '/gmail/v1/users/%s/threads/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('DELETE');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| delete - 
  method delete (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= ID of the Thread to delete.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
    >);
    my $function = sprintf '/gmail/v1/users/%s/threads/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('DELETE');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - 
  method get (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the thread to retrieve.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
      https://www.googleapis.com/auth/gmail.metadata
    >);
    my $function = sprintf '/gmail/v1/users/%s/threads/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - 
  method get (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the thread to retrieve.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
      https://www.googleapis.com/auth/gmail.metadata
    >);
    my $function = sprintf '/gmail/v1/users/%s/threads/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| list - 
  method list (
    Str() $userId  #= The user's email address. The special value me can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
      https://www.googleapis.com/auth/gmail.metadata
    >);
    my $function = sprintf '/gmail/v1/users/%s/threads',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| list - 
  method list (
    Str() $userId  #= The user's email address. The special value me can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
      https://www.googleapis.com/auth/gmail.metadata
    >);
    my $function = sprintf '/gmail/v1/users/%s/threads',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| modify - 
  method modify (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the thread to modify.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
    >);
    my $function = sprintf '/gmail/v1/users/%s/threads/%s/modify',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| modify - 
  method modify (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the thread to modify.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
    >);
    my $function = sprintf '/gmail/v1/users/%s/threads/%s/modify',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| trash - 
  method trash (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the thread to Trash.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
    >);
    my $function = sprintf '/gmail/v1/users/%s/threads/%s/trash',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| trash - 
  method trash (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the thread to Trash.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
    >);
    my $function = sprintf '/gmail/v1/users/%s/threads/%s/trash',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| untrash - 
  method untrash (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the thread to remove from Trash.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
    >);
    my $function = sprintf '/gmail/v1/users/%s/threads/%s/untrash',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| untrash - 
  method untrash (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the thread to remove from Trash.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
    >);
    my $function = sprintf '/gmail/v1/users/%s/threads/%s/untrash',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
