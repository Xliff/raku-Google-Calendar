use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = 'https://www.googleapis.com/auth/';  # auth-url
constant DRAFTS_URL_FORMAT = 'https://gmail.googleapis.com/gmail/v1/users'; # url-format

class Google::Mail::Drafts is Google::Calendar::Base  {
  has $!prefix is built;

  submethod BUILD ( :$!prefix ) {
  }

  method class-url-format {
    DRAFTS_URL_FORMAT
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      DRAFTS_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| create - DRAFT
  method create (
    Str() $userId  #= The user's email address. The special value me can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.compose
    >);
    my $function = sprintf '/gmail/v1/users/%s/drafts',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| create - DRAFT
  method create (
    Str() $userId  #= The user's email address. The special value me can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.compose
    >);
    my $function = sprintf '/gmail/v1/users/%s/drafts',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| delete - 
  method delete (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the draft to delete.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.compose
    >);
    my $function = sprintf '/gmail/v1/users/%s/drafts/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('DELETE');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| delete - 
  method delete (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the draft to delete.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.compose
    >);
    my $function = sprintf '/gmail/v1/users/%s/drafts/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('DELETE');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - 
  method get (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the draft to retrieve.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.compose
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/drafts/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| get - 
  method get (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the draft to retrieve.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.compose
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/drafts/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| list - 
  method list (
    Str() $userId  #= The user's email address. The special value me can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.compose
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/drafts',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| list - 
  method list (
    Str() $userId  #= The user's email address. The special value me can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.compose
      https://www.googleapis.com/auth/gmail.readonly
    >);
    my $function = sprintf '/gmail/v1/users/%s/drafts',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| send - Bcc
  method send (
    Str() $userId  #= The user's email address. The special value me can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.compose
    >);
    my $function = sprintf '/gmail/v1/users/%s/drafts/send',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| send - Bcc
  method send (
    Str() $userId  #= The user's email address. The special value me can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.compose
    >);
    my $function = sprintf '/gmail/v1/users/%s/drafts/send',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('POST');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| update - 
  method update (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the draft to update.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.compose
    >);
    my $function = sprintf '/gmail/v1/users/%s/drafts/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| update - 
  method update (
    Str() $userId,  #= The user's email address. The special value me can be used to indicate the authenticated user.
     Str() $id  #= The ID of the draft to update.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.compose
    >);
    my $function = sprintf '/gmail/v1/users/%s/drafts/%s',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('PUT');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
