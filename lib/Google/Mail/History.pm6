use v6;

use JSON::Fast;
use Rest::OAuth2::Proxy;

constant GOOGLE_AUTH_URL = 'https://www.googleapis.com/auth/';  # auth-url
constant HISTORY_URL_FORMAT = 'https://gmail.googleapis.com/gmail/v1/users'; # url-format

class Google::Mail::History is Google::Calendar::Base  {
  has $!prefix is built;

  submethod BUILD ( :$!prefix ) {
  }

  method class-url-format {
    HISTORY_URL_FORMAT
  }

  method new ($client-id, $prefix) {
    my $proxy = Rest::OAuth2::Proxy.new(
      $client-id,
      GOOGLE_AUTH_URL,
      HISTORY_URL_FORMAT
    );

    $proxy ?? self.bless( :$proxy, :$prefix ) !! Nil;
  }

   #| list - 
  method list (
    Str() $userId  #= The user's email address. The special value me can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
      https://www.googleapis.com/auth/gmail.metadata
    >);
    my $function = sprintf '/gmail/v1/users/%s/history',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }

   #| list - 
  method list (
    Str() $userId  #= The user's email address. The special value me can be used to indicate the authenticated user.
  ) {
    return unless self.checkScopes(<
      https://mail.google.com/
      https://www.googleapis.com/auth/gmail.modify
      https://www.googleapis.com/auth/gmail.readonly
      https://www.googleapis.com/auth/gmail.metadata
    >);
    my $function = sprintf '/gmail/v1/users/%s/history',
                           |&?ROUTINE.signature
                                     .params
                                     .grep( *.named.not )
                                     .map( ::( *.name ) );
    
    my $call = $!p.new-call;
    $call.set-method('GET');
    $call.set-function($function);
    
    $call.sync;
    from-json( $call.payload );
  }
}
